//
//  SearchTrainInteractorTests.swift
//  MyTravelHelperTests
//
//  Created by Apple on 17/09/21.
//  Copyright © 2021 Sample. All rights reserved.
//

import XCTest

@testable import MyTravelHelper

class SearchTrainInteractorTests: XCTestCase {
    
    var searchInteractor : SearchTrainInteractor!
    var presenter = searchTrainPresenterMock()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        searchInteractor = SearchTrainInteractor()
        searchInteractor.presenter = presenter
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testFetchTrainList() {
        let expectation = expectation(description: "fetch train from source")
        presenter.expectation = expectation
        searchInteractor.fetchTrainsFromSource(sourceCode: "Lurgan", destinationCode: "Sligo")
        wait(for: [expectation], timeout: 60)
    }

    func testFetchAllLocations(){
        let expectation = expectation(description: "fetch all stations")
        presenter.expectation = expectation
        searchInteractor.fetchallStations()
        wait(for: [expectation], timeout: 60)
        
    }
    

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

class searchTrainPresenterMock : ViewToPresenterProtocol, InteractorToPresenterProtocol {
    
    var expectation: XCTestExpectation?
    
    var view: PresenterToViewProtocol?
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    
    func fetchallStations() {
        
    }
    
    func searchTapped(source: String, destination: String) {
        
    }
    
    func savefavouriteStation(station: String) {
        
    }
    
    func getFavStation() -> String? {
        ""
    }
    
    func stationListFetched(list: [Station]) {
        XCTAssertNotNil(list)
        expectation?.fulfill()
    }
    
    func fetchedTrainsList(trainsList: [StationTrain]?) {
        XCTAssertNotNil(trainsList)
        expectation?.fulfill()
    }
    
    func showNoTrainAvailbilityFromSource() {
        
    }
    
    func showNoInterNetAvailabilityMessage() {
        
    }
    
    
}
