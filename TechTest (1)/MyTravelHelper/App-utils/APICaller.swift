//
//  APICaller.swift
//  MyTravelHelper
//
//  Created by Apple on 15/09/21.
//  Copyright © 2021 Sample. All rights reserved.
//

import Foundation
import XMLParsing



func fetchData<T: Decodable>(urlString: String, expecting: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
    guard let url = URL(string: urlString) else { return } // or throw an error
    let task = URLSession.shared.dataTask(with: url){ (data, response, error) in

        if let errorp = error { completion(.failure(errorp)); return }
        do{
            let model = try XMLDecoder().decode(T.self, from: data!)
            completion(.success(model))
        }catch{
            completion(.failure(error))
        }
    }
    task.resume()
}
